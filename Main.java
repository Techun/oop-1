public class Main {
    public static void main(String[] args) {
        //task1
        System.out.println("\nTASK1");
        task1 task = new task1(5, 10);
        task.display();
        task.setVar1(12);
        System.out.println(task.amount());
        System.out.println(task.max());

        //task2
        System.out.println("\nTASK2");
        task2 task2= new task2(2,3);
        task2.setVar1(14);
        System.out.println(task2.getVar1());

        //task3
        System.out.println("\nTASK3");
        Triangle task3 = new Triangle(3,4,5);
        System.out.println(task3.Area());
        System.out.println(task3.Perimeter());

        //task4
        System.out.println("\nTASK4");
        DecimalCounter task4 = new DecimalCounter(0,100,0);
        task4.increase();
        System.out.println(task4.getCurrentValue());
        task4.decrease();
        System.out.println(task4.getCurrentValue());

        //task5
        System.out.println("\nTASK5");
        TimeRepresentation currentTime = new TimeRepresentation();

        currentTime.setHour(15);
        currentTime.setMinute(30);
        currentTime.setSecond(45);

        System.out.println("Current time: " + currentTime.getHour() + ":" + currentTime.getMinute() + ":" + currentTime.getSecond());

        currentTime.addHours(3);
        currentTime.addMinutes(5);
        currentTime.addSeconds(60);

        System.out.println("New time: " + currentTime.getHour() + ":" + currentTime.getMinute() + ":" + currentTime.getSecond());
    }
}
