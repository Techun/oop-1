public class DecimalCounter {
    private final int min;
    private final int max;
    private int currentValue;

    public DecimalCounter(int min, int max, int currentValue) {
        this.min = min;
        this.max = max;
        this.currentValue = currentValue;
    }

    public DecimalCounter() {
        min = 0;
        max = 100;
        currentValue = 0;
    }

    public void increase() {
        if (currentValue < max) {
            currentValue++;
        }
    }

    public void decrease() {
        if (currentValue > min) {
            currentValue--;
        }
    }


    public int getCurrentValue() {
        return currentValue;
    }
}
