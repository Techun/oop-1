public class Triangle {
    private final double side1;
    private final double side2;
    private final double side3;

    public Triangle(double side1, double side2, double side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public double Area() {
        double p2 = (side1 + side2 + side3) / 2;
        return Math.sqrt(p2 * (p2 - side1) * (p2 - side2) * (p2 - side3));
    }

    public double Perimeter() {
        return side1 + side2 + side3;
    }
}
