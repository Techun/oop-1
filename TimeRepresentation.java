public class TimeRepresentation {
    private int hour;
    private int minute;
    private int second;

    public TimeRepresentation() {
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        if (hour >= 0 && hour < 24) {
            this.hour = hour;
        } else {
            this.hour = 0;
        }
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        if (minute >= 0 && minute < 60) {
            this.minute = minute;
        } else {
            this.minute = 0;
        }
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        if (second >= 0 && second < 60) {
            this.second = second;
        } else {
            this.second = 0;
        }
    }

    public void addHours(int hours) {
        int totalHours = this.hour + hours;
        this.hour = totalHours % 24;
    }

    public void addMinutes(int minutes) {
        int totalMinutes = this.minute + minutes;
        int additionalHours = totalMinutes / 60;
        this.minute = totalMinutes % 60;
        addHours(additionalHours);
    }

    public void addSeconds(int seconds) {
        int totalSeconds = this.second + seconds;
        int additionalMinutes = totalSeconds / 60;
        this.second = totalSeconds % 60;
        addMinutes(additionalMinutes);
    }
}
