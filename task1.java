public class task1 {
    private int var1;
    private int var2;

    public task1(int var1, int var2) {
        this.var1 = var1;
        this.var2 = var2;
    }

    public void display() {
        System.out.println("Var1: " + var1);
        System.out.println("Var2: " + var2);
    }

    public void setVar1(int var1) {
        this.var1 = var1;
    }

    public void setVar2(int var2) {
        this.var2 = var2;
    }

    public int amount() {
        return var1 + var2;
    }

    public int max() {
        return Math.max(var1, var2);
    }
}
